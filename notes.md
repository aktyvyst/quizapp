

Firebase kurz vorstellen, Projekt zeigen

--- firebase installieren

npm i firebase
firebase util file erstellen mit credentials und:

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
auth.setPersistence(firebase.auth.Auth.Persistence.NONE)

--- Login Screen

State einbauen, value und onChangeText

--- React Context

export const UserContext = createContext();

user im state

await auth.signInWithEmailAndPassword(email, password)

await auth.createUserWithEmailAndPassword(email, password);

await auth.signOut()

useEffect(() => {
    const unsubscribe = auth.onIdTokenChanged((newUser) => {
      console.log('onIdTokenChanged')
      setUser(newUser)
    });
    return unsubscribe;
  }, [])

  return (
    <UserContext.Provider value={{
      user,
      signIn,
      signOut,
      signUp
    }}>
      {props.children}
    </UserContext.Provider>
  )

--- Context Consumer

  import { UserContext, UserContextProvider } from "./src/context/User";

  App.js

   <UserContextProvider>
      <UserContext.Consumer>
        {({ user }) => {
          if (user) {
            return (
              <NavigationContainer>

--- profile api

  api/profile.js

  axios installieren

  apiHost schicken

  axios.defaults.baseURL = Config.apiHost;

  import { auth } from '../util/firebase';

  const user = auth.currentUser;
    if (!user) { throw new Error('User not logged in') }
    const token = await auth.currentUser.getIdToken(true);
    const response = await axios.get('/profile', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    
--- Profile/Details.js

    import { UserContext } from "../../context/User";
    useContext
    const { user, signOut } = useContext(UserContext);
    onPress={() => signOut()}

    <Avatar
    ...
    source={{ uri: profile?.picture?.large }}
    ...
    > 

    {`${profile?.name?.first} ${profile?.name?.last}`}
    {`${profile?.location?.street?.name} ${profile?.location?.street?.number}`}
    {`${profile?.nat}-${profile?.location?.postcode} ${profile?.location?.city}`}

    Logout/Login zeigen
    Hinweis auf asyncStorage

--- Quizliste

  quiz api
  getQuizzes
  Route /quiz
  axios.defaults.baseURL = Config.apiHost;
  'Content-Type': 'application/json',

    <ListItem.Title style={appStyles.boldText}>{quiz.name}</ListItem.Title>
    
    {
      quiz.openQuiz === false ?
        <ListItem.Chevron type="font-awesome" name="lock" />
        : <ListItem.Chevron />
    }

    selectQuiz in props

    <ListItem key={quiz._id} bottomDivider onPress={() => selectQuiz(index)}>

    -Pause-

index.js

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [quizzes, setQuizzes] = useState([]);
  const [loading, setLoading] = useState([]);
  const [activeQuiz, setActiveQuiz] = useState(null);
  const [shouldUpdate, setShouldUpdate] = useState(true);

    const selectQuiz = (index) => {
    if (quizzes[index].openQuiz === true) {
      setActiveQuiz({
        quiz: quizzes[index],
        activeQuestion: 0,
        finished: false,
        result: 0
      });
      setSelectedIndex(1);
    } else {
      alert('Dieses Quiz ist noch nicht freigegeben')
    }
  };

  nextQuestionHandler
  refreshQuizList (Button und als Prop für QuestionView)

  const nextQuestion = (questionResult) => {
    if (activeQuiz !== null) {
      if (activeQuiz.activeQuestion < activeQuiz.quiz.questions.length - 1) {
        setActiveQuiz({
          ...activeQuiz,
          activeQuestion: activeQuiz.activeQuestion + 1,
          result: activeQuiz.result + questionResult
        })
      } else {
        setActiveQuiz({
          ...activeQuiz,
          finished: true
        })
      }
    }
  }

  <Button title="Neu laden" type="clear" onPress={() => { refresh() }} />
  in QuizList.js

  



