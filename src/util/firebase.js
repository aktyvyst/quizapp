import firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyCQc1NIHHfw81BKw6JUqyaHPfYNKgWWdpA",
  authDomain: "quizapp-48b67.firebaseapp.com",
  projectId: "quizapp-48b67",
  storageBucket: "quizapp-48b67.appspot.com",
  messagingSenderId: "139564026332",
  appId: "1:139564026332:web:676cba5740f8fd2590e719"
}
firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
auth.setPersistence(firebase.auth.Auth.Persistence.NONE)
