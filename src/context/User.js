import React, { useEffect, useState, createContext } from 'react';
import { auth } from '../util/firebase';

export const UserContext = createContext();

export const UserContextProvider = (props) => {
  const [user, setUser] = useState(null);
  useEffect(() => {
    const unsubscribe = auth.onIdTokenChanged((newUser) => {
      console.log('onIdTokenChanged')
      setUser(newUser)
    });
    return unsubscribe;
  }, [])
  const signIn = async (email, password) => {
    try {
      await auth.signInWithEmailAndPassword(email, password)
    } catch (err) {
      alert(`Login fehlgeschlagen: ${err.code}`)
    }
  }
  const signUp = async (email, password) => {
    try {
      await auth.createUserWithEmailAndPassword(email, password);
    } catch (err) {
      alert(`Registrierung fehlgeschlagen: ${err.code}`)
    }
  }
  const signOut = async () => {
    try {
      await auth.signOut()
    } catch (err) {
      alert(`Logout fehlgeschlagen: ${err.code}`)
    }
  }
  return (
    <UserContext.Provider value={{
      user,
      signIn,
      signOut,
      signUp
    }}>
      {props.children}
    </UserContext.Provider>
  )
}