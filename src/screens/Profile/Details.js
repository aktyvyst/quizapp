import React, { useContext, useEffect, useState } from "react";
import { Button, Avatar, Divider } from "react-native-elements";
import { View, Text, ActivityIndicator } from "react-native";
import { UserContext } from "../../context/User";
import { appStyles, appColors } from "../../styles";
import { getProfile } from "../../api/profile";
import Login from "./Login";

const DetailScreen = (props) => {
  const { navigate } = props.navigation;
  const { user, signOut } = useContext(UserContext);
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    const loadProfile = async () => {
      try {
        const data = await getProfile();
        setProfile(data);
        console.log(profile);
      } catch (err) {
        alert("getProfile failed");
      }
    };
    if (user) {
      loadProfile();
    }
  }, [user]);

  if (!user) {
    return <Login />;
  }
  if (profile === null) {
    return <ActivityIndicator size="large" style={{ flex: 1 }} />;
  } else {
    return (
      <View style={{ ...appStyles.container, justifyContent: "flex-start" }}>
        <Avatar
          rounded
          size="large"
          source={{ uri: profile?.picture?.large }}
          // overlayContainerStyle={{ backgroundColor: "white" }}
          onPress={() => console.log("Works!")}
          activeOpacity={1}
          containerStyle={{
            //   marginTop: 75,
            width: 80,
            height: 80,
            alignSelf: "center",
            backgroundColor: "whitesmoke"
          }}
        />
        <Divider
          style={{
            backgroundColor: appColors.main,

            margin: 20
          }}
        />
        <Text style={appStyles.boldText}>Name</Text>
        <Text
          style={{ ...appStyles.standardText }}
        >{`${profile?.name?.first} ${profile?.name?.last}`}</Text>
        <Text style={appStyles.boldText}>Adresse</Text>
        <Text
          style={{ ...appStyles.standardText }}
        >{`${profile?.location?.street?.name} ${profile?.location?.street?.number}`}</Text>
        <Text style={appStyles.boldText}>Ort</Text>
        <Text
          style={{ ...appStyles.standardText }}
        >{`${profile?.nat}-${profile?.location?.postcode} ${profile?.location?.city}`}</Text>
        <Button
          style={{ ...appStyles.button, marginTop: 100 }}
          onPress={() => signOut()}
          title={"Logout"}
        />
      </View>
    );
  }
};

export default DetailScreen;
