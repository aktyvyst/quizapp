import React, { useState, useContext } from "react";
import { Input, Button, Icon } from "react-native-elements";
import { appStyles, appColors } from "../../styles";
import { View, SafeAreaView } from "react-native";
import { UserContext } from "../../context/User";

const LoginScreen = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { user, signIn, signOut, signUp } = useContext(UserContext);

  return (
    <SafeAreaView style={appStyles.safeArea}>
      <View style={appStyles.container}>
        <Input
          value={email}
          leftIcon={
            <Icon
              name="email"
              type="material-community"
              size={24}
              color={appColors.gray}
            />
          }
          onChangeText={(value) => {
            setEmail(value);
          }}
          autoCorrect={false}
          autoCapitalize="none"
          placeholder={"Deine E-Mail Adresse"}
          style={appStyles.standardInput}
        />

        <Input
          value={password}
          onChangeText={(value) => {
            setPassword(value);
          }}
          secureTextEntry={true}
          leftIcon={
            <Icon
              name="user-secret"
              type="font-awesome"
              size={24}
              color={appColors.gray}
            />
          }
          autoCorrect={false}
          autoCapitalize="none"
          placeholder={"Dein Passwort"}
          style={appStyles.standardInput}
        />

        <Button
          title="Login"
          onPress={() => signIn(email, password)}
          buttonStyle={{ ...appStyles.button }}
        />

        <Button
          title="Registrierung"
          onPress={() => signUp(email, password)}
          buttonStyle={{
            ...appStyles.button,
            backgroundColor: appColors.secondary
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;
