import React, { useContext, useEffect, useState } from "react";
import { Button, Avatar, Icon } from "react-native-elements";
import { View, Text } from "react-native";
import { UserContext } from "../../context/User";
import { appStyles, appColors } from "../../styles";
import { getProfile } from "../../api/profile";
import Login from "./Login";

const Profile = (props) => {
  const { navigate } = props.navigation;
  const { user, signOut } = useContext(UserContext);
  const [profile, setProfile] = useState({});

  useEffect(() => {
    const loadProfile = async () => {
      try {
        const data = await getProfile();
        setProfile(data);
      } catch (err) {
        alert("getProfile failed");
      }
    };
    if (user) {
      loadProfile();
    }
  }, [user]);

  if (!user) {
    return <Login />;
  }
  return (
    <View style={{ ...appStyles.container }}>
      <Text style={appStyles.standardText}>
        {user
          ? `Hallo, Du bis angemeldet als ${user.email}.`
          : "Nicht angemeldet."}
      </Text>
      <Text
        onPress={() => navigate("Details")}
        style={{ ...appStyles.standardText, textDecorationLine: "underline" }}
      >
        Details
      </Text>
      <Button
        style={{ ...appStyles.button, marginTop: 100 }}
        onPress={() => signOut()}
        title={"Logout"}
      />
    </View>
  );
};

export default Profile;
