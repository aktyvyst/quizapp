export const quizzerData = [
  {
    name: "Amy Farha",
    avatar_url:
      "https://s.gravatar.com/avatar/8338192df8a649d3fdfcbe9d325e4f44?size=496&default=retro",
    subtitle: "Vice President",
    points: 1
  },
  {
    name: "Chris Jackson",
    avatar_url:
      "https://s.gravatar.com/avatar/8338192df8a649d3fdfcbe9d325e4f44?size=496&default=retro",
    subtitle: "Freak",
    points: 3
  },
  {
    name: "Jana Doe",
    avatar_url:
      "https://s.gravatar.com/avatar/8338192df8a649d3fdfcbe9d325e4f44?size=496&default=retro",
    subtitle: "Vice President",
    points: 2
  },
  {
    name: "Pete York",
    avatar_url:
      "https://s.gravatar.com/avatar/8338192df8a649d3fdfcbe9d325e4f44?size=496&default=retro",
    subtitle: "Drummer",
    points: 4
  }
];
export const mockedQuestion = {
  text: "Wie heißt der Bürgermeister von Wesel?",
  num: 1,
  answers: [
    { text: "Dieter Hallervorden" },
    { text: "Pinocchio" },
    { text: "Quentin Tarantino" },
    { text: "Esel" }
  ],
  correctAnswer: 3
};