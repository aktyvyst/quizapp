import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  ScrollView,
  LayoutAnimation,
  UIManager,
  Platform,
  Animated
} from "react-native";
import { Button, Badge } from "react-native-elements";
import AnswerButton from "./AnswerButton";

import { appStyles } from "../../styles";
if (
  Platform.OS === "android" &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}
const QuestionView = (props) => {
  const { question, nextQuestion, questionNumber, finished, totalResult, saveResult } = props;
  const countDownScaleValue = useRef(new Animated.Value(2.0));
  const [timedOut, setTimedOut] = useState(false);
  // const [options, setOptions] = useState([]);
  const [selectedAnswer, setSelectedAnswer] = useState(-1);
  const [timerValue, setTimerValue] = useState(20);

  // useEffect(() => {
  //   if (question !== null && selectedAnswer == -1) {
  //     LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  //     setOptions(question.options);
  //   }
  // }, [question]);

  useEffect(() => {
    console.log('New Question', question)
    setTimedOut(false);
    // setOptions(question.options)
    setTimerValue(20);
    setSelectedAnswer(-1)
  }, [question]);


  useEffect(() => {
    let timer = null;
    if (timerValue >= 1 && selectedAnswer < 0) {
      timer = setTimeout(() => {
        setTimerValue(timerValue - 1);
      }, 1000);
      countDownScaleValue.current.setValue(2);
      animateCountdown();
    } else {
      setTimedOut(true);
    }
    return () => {
      if (timer !== null) {
        clearTimeout(timer);
      }
    }
  }, [timerValue, selectedAnswer]);
  const animateCountdown = () => {
    Animated.spring(countDownScaleValue.current, {
      toValue: 1,
      useNativeDriver: true
    }).start();
  };

  return (
    <View style={{ ...appStyles.container, flex: 1 }}>
      <View style={appStyles.questionContainer}>
        <Badge
          value={questionNumber}
          containerStyle={{
            flex: 1,
            marginTop: 8,
            height: 30,
            width: 30
          }}
          badgeStyle={{ width: 30, height: 30, borderRadius: 15 }}
        />

        <Text
          style={{
            ...appStyles.questionHeadline
          }}
          numberOfLines={3}
        >
          {question.question}
        </Text>
        <Animated.Text
          style={{
            ...appStyles.countdown,

            transform: [
              { scale: countDownScaleValue.current }
              // { scaleY: ticketCountScale }
            ],
            opacity: countDownScaleValue.current.interpolate({
              outputRange: [1, 0.2],
              inputRange: [1, 2]
            })
          }}
          selectable={false}
        >
          {timerValue}
        </Animated.Text>
      </View>

      <ScrollView style={{ backgroundColor: "transparent" }}>
        {
          question.options.map((option, i) => {
            const disabled = selectedAnswer > -1 || timedOut ? true : false;
            return (
              <AnswerButton
                key={`answer-${i}`}
                selectAnswerHandler={setSelectedAnswer}
                title={option}
                disabled={disabled}
                disabledStyling={!(selectedAnswer === i)}
                questionIndex={i}
                isCorrect={
                  selectedAnswer === i &&
                  selectedAnswer === question.answer
                }
              />
            );
          })
        }


        {finished === true ?
          <React.Fragment>
            <Text style={{ alignSelf: 'center' }}>Ergebnis: {totalResult} Punkte</Text>
            <Button
              type="clear"
              title="Speichern"
              onPress={() => { saveResult() }}
            />
          </React.Fragment>
          : <Button
            title="Weiter"
            type="clear"
            onPress={() => {
              if (selectedAnswer === question.answer) {
                nextQuestion(timerValue + 1);
              } else {
                nextQuestion(0);
              }
            }}
            disabled={!(selectedAnswer > -1) && !timedOut}
          />
        }

      </ScrollView>
    </View>
  );
};

export default QuestionView;
