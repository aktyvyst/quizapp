import React, { useEffect, useState } from "react";
import {
  FlatList,
  Text
} from "react-native";
import { Button, ListItem, Avatar } from "react-native-elements";
import { appStyles } from "../../styles";
import { getLeaderBoard } from '../../api/quiz';


const Leaderboard = (props) => {
  const { quizId } = props;
  const [leaderboard, setLeaderboard] = useState([]);

  useEffect(() => {
    const fetchLeaderboard = async () => {
      try {
        const data = await getLeaderBoard(quizId);
        setLeaderboard(data);
        console.log(data);
      } catch (err) {
        setLeaderboard([]);
        alert(err.message);
      }
    }
    fetchLeaderboard();
  }, [quizId])

  const Item = ({ email, result }) => (
    <ListItem bottomDivider>
      {/* <Avatar rounded source={{ uri: quizzer.avatar_url }} /> */}
      {/* <Avatar rounded /> */}
      <ListItem.Content>
        <ListItem.Title style={appStyles.boldText}>
          {email}
        </ListItem.Title>
        <ListItem.Subtitle style={appStyles.standardText}>
          Punkte: {result}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );

  const renderItem = ({ item }) => <Item email={item.email} result={item.result} />

  return (
    <React.Fragment>
      <FlatList
        data={leaderboard}
        renderItem={renderItem}
        keyExtractor={(item) => `${item.email}`}
        style={{ width: "90%", alignSelf: "center" }}
      />
      <Button title="Neu laden" type="clear" onPress={() => { }} />
    </React.Fragment>
  );
};

export default Leaderboard;
