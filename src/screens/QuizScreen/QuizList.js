import React, { useState, useEffect, useRef } from "react";
import { View, Text } from "react-native";
import { ListItem, Button } from "react-native-elements";
import { appStyles } from "../../styles";



const QuizList = ({ quizzes, selectQuiz, refresh }) => {


  const quizList = quizzes.map((quiz, index) => {
    return (
      <ListItem key={quiz._id} bottomDivider onPress={() => selectQuiz(index)}>
        <ListItem.Content>
          <ListItem.Title style={appStyles.boldText}>{quiz.name}</ListItem.Title>
        </ListItem.Content>
        {
          quiz.openQuiz === false ?
            <ListItem.Chevron type="font-awesome" name="lock" />
            : <ListItem.Chevron />
        }

      </ListItem>
    )
  })

  return (
    <View style={{ ...appStyles.container, justifyContent: "flex-start" }}>
      {quizList}
      <Button title="Neu laden" type="clear" onPress={() => { refresh() }} />
    </View>
  );
};

export default QuizList;
