import React from "react";
import { Button } from "react-native-elements";
import { appStyles, appColors } from "../../styles";

const AnswerButton = (props) => {
  const {
    questionIndex,
    selectAnswerHandler,
    title,
    disabled,
    disabledStyling,
    isCorrect
  } = props;
  const shadowed =
    !disabledStyling && !disabled
      ? {
        shadowColor: appColors.gray,
        shadowOffset: { width: 2, height: 3 },
        shadowOpacity: 0.7
      }
      : {};
  return (
    <Button
      key={`question-${questionIndex}`}
      onPress={() => selectAnswerHandler(questionIndex)}
      title={title}
      titleStyle={{
        ...appStyles.quizButtonTitle
      }}
      disabled={disabled}
      disabledStyle={{
        backgroundColor: isCorrect
          ? "green"
          : !disabledStyling
            ? "red"
            : "none",

        borderWidth: !disabledStyling ? 0 : 1
      }}
      disabledTitleStyle={{
        color: !disabledStyling ? "white" : appColors.gray
      }}
      buttonStyle={{
        ...appStyles.quizButton,
        ...shadowed
      }}
    />
  );
};

export default AnswerButton;
