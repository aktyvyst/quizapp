import React, { useEffect, useState } from "react";
import { ButtonGroup } from "react-native-elements";
import { SafeAreaView, View, Text } from "react-native";
import { UserContext } from "../../context/User";
import { appStyles } from "../../styles";
import QuestionView from "./QuestionView";
import QuizList from "./QuizList";
import Leaderboard from "./Leaderboard";
import { mockedQuestion, quizzerData } from './mocks';
import { getQuizzes } from '../../api/quiz';
import { postResult } from '../../api/quiz';


const QuizScreen = (props) => {
  const { question, options } = props;
  const buttons = ["Auswahl", "Quiz", "Leaderboard"];
  const [selectedIndex, setSelectedIndex] = useState(0);

  const [quizzes, setQuizzes] = useState([]);
  const [loading, setLoading] = useState([]);
  const [activeQuiz, setActiveQuiz] = useState(null);
  const [shouldUpdate, setShouldUpdate] = useState(true);
  // const [activeQuestion, setActiveQuestion] = useState(0);

  const selectQuiz = (index) => {
    if (quizzes[index].openQuiz === true) {
      setActiveQuiz({
        quiz: quizzes[index],
        activeQuestion: 0,
        finished: false,
        result: 0
      });
      setSelectedIndex(1);
    } else {
      alert('Dieses Quiz ist noch nicht freigegeben')
    }
  };

  const refreshQuizList = () => {
    setShouldUpdate(true);
  }

  const saveResult = async () => {
    try {
      await postResult(activeQuiz.quiz._id, activeQuiz.result);
      setSelectedIndex(2);
    } catch (err) {
      alert(err);
    }
  }

  const nextQuestion = (questionResult) => {
    if (activeQuiz !== null) {
      if (activeQuiz.activeQuestion < activeQuiz.quiz.questions.length - 1) {
        setActiveQuiz({
          ...activeQuiz,
          activeQuestion: activeQuiz.activeQuestion + 1,
          result: activeQuiz.result + questionResult
        })
      } else {
        setActiveQuiz({
          ...activeQuiz,
          finished: true
        })
      }
    }
  }

  useEffect(() => {
    const fetchQuizzes = async () => {
      try {
        setLoading(true);
        const data = await getQuizzes();
        setQuizzes(data);
      } catch (err) {
        setQuizzes([]);
        alert(err);
      } finally {
        setLoading(false);
        setShouldUpdate(false);
      }
    }
    if (shouldUpdate) {
      fetchQuizzes();
    }
  }, [shouldUpdate])



  const selectedComponent = () => {
    switch (selectedIndex) {
      case 0:
        return <QuizList quizzes={quizzes} selectQuiz={selectQuiz} refresh={refreshQuizList} />;
        break;
      case 1:
        if (activeQuiz !== null) {
          return <QuestionView
            question={activeQuiz.quiz.questions[activeQuiz.activeQuestion]}
            nextQuestion={nextQuestion}
            questionNumber={activeQuiz.activeQuestion + 1}
            finished={activeQuiz.finished}
            totalResult={activeQuiz.result}
            saveResult={saveResult}
          />;
        } else {
          return <View><Text>Bitte wähle ein Quiz aus</Text></View>
        }
        break;
      case 2:
        return <Leaderboard quizId={activeQuiz.quiz._id} />;
        break;
      default:
        return <QuizList />;
        break;
    }
    return;
  };


  // SafeAreaView erstmal weg, dann zeigen
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ButtonGroup
        onPress={(i) => setSelectedIndex(i)}
        containerStyle={{ height: 40, borderRadius: 8, marginTop: 20 }}
        selectedIndex={selectedIndex}
        buttons={buttons}
        disabled={activeQuiz !== null ? [] : [1, 2]}
      />
      {selectedComponent()}
    </SafeAreaView>
  );
};

export default QuizScreen;
