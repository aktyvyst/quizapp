import React from "react";
import { StyleSheet, Text, View, Image, Linking } from "react-native";
import { StatusBar } from "expo-status-bar";
// StyleSheet vs. "simple" style object
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: "flex-start",
    justifyContent: "center",
    backgroundColor: "#fff"
  },
  Headline: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    color: "#14143D"
  },
  defaultText: {
    fontSize: 16,
    color: "#4196F6",
    margin: 20,
    textAlign: "center"
  },
  imageStyle: {
    // ...StyleSheet.absoluteFill,
    width: 80,
    height: 80,
    alignSelf: "center",
    resizeMode: "contain",
    marginTop: 40
  }
});
const Home = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.Headline}>
        Willkommen beim React Native QuizApp Workshop!
      </Text>
      <Image
        source={require("../../../assets/actLogo_sign.png")}
        style={styles.imageStyle}
      />
      <Text style={styles.defaultText}>
        {`18.03.2021\n React Native Hands-On mit Ralf und Thomas,\n`}
        <Text
          onPress={() => {
            Linking.openURL("http://actyvyst.de");
          }}
          style={{ ...styles.defaultText, textDecorationLine: "underline" }}
        >
          actyvyst GmbH
        </Text>
      </Text>

      <StatusBar style="auto" />
    </View>
  );
};

export default Home;
