import axios from 'axios';
import { auth } from '../util/firebase';
import Config from '../config';

axios.defaults.baseURL = Config.apiHost;

export const getProfile = async () => {
  try {
    const user = auth.currentUser;
    if (!user) { throw new Error('User not logged in') }
    const token = await auth.currentUser.getIdToken(true);
    const response = await axios.get('/profile', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    return response.data;
  } catch (err) {
    console.log('API error')
    if (err.response) {
      console.log(err.response.status)
      console.log(err.response.data)
    } else {
      console.log(err);
    }
    throw err;
  }
}
