import axios from 'axios';
import { auth } from '../util/firebase';
import Config from '../config';

axios.defaults.baseURL = Config.apiHost;


export const getQuizzes = async () => {
  try {
    const response = await axios.get('/quiz', {
      headers: {
        'Content-Type': 'application/json',
      }
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw (err);
  }
}

export const postResult = async (quizId, result) => {
  try {
    const user = auth.currentUser;
    if (!user) { throw new Error('User not logged in') }
    const token = await auth.currentUser.getIdToken(true);
    await axios.post('/score', { quizId, result }, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
  } catch (err) {
    console.log(err);
    throw (err);
  }
}

export const getLeaderBoard = async (quizId) => {
  try {
    const response = await axios.get(`/leaderboard?quiz=${quizId}`, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw (err);
  }
}

