import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Profile from "./src/screens/Profile";
import Detail from "./src/screens/Profile/Details";
import Login from "./src/screens/Profile/Login";
import Home from "./src/screens/Home";
import QuizScreen from "./src/screens/QuizScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { UserContext, UserContextProvider } from "./src/context/User";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
const App = (props) => {
  const Tab = createBottomTabNavigator();
  const pStack = createStackNavigator();
  const ProfileStackScreen = () => {
    return (
      <pStack.Navigator>
        <pStack.Screen name="Profil" component={Profile} />
        <pStack.Screen name="Details" component={Detail} />
      </pStack.Navigator>
    );
  };
  return (
    <UserContextProvider>
      <UserContext.Consumer>
        {({ user }) => {
          if (user) {
            return (
              <NavigationContainer>
                <Tab.Navigator
                  screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                      let iconName;
                      let iconComponent;
                      switch (route.name) {
                        case "Home":
                          iconName = focused ? "ios-home" : "ios-home-outline";
                          iconComponent = (
                            <Ionicons
                              name={iconName}
                              size={size}
                              color={color}
                            />
                          );
                          break;
                        case "Quiz":
                          iconName = focused
                            ? "comment-question"
                            : "comment-question-outline";
                          iconComponent = (
                            <MaterialCommunityIcons
                              name={iconName}
                              size={size}
                              color={color}
                            />
                          );
                          break;
                        case "Account":
                          iconName = focused
                            ? "account-settings"
                            : "account-settings-outline";
                          iconComponent = (
                            <MaterialCommunityIcons
                              name={iconName}
                              size={size}
                              color={color}
                            />
                          );
                          break;
                      }
                      return iconComponent;
                    }
                  })}
                  tabBarOptions={{
                    activeTintColor: "#4196F6",
                    inactiveTintColor: "gray"
                  }}
                >
                  <Tab.Screen
                    name="Home"
                    component={Home}
                    options={{ title: "Home" }}
                  />
                  <Tab.Screen
                    name="Quiz"
                    component={QuizScreen}
                    options={({ route }) => ({ title: route.name })}
                  />
                  <Tab.Screen
                    name="Account"
                    component={ProfileStackScreen}
                    options={{ title: "Profil" }}
                  />
                </Tab.Navigator>
              </NavigationContainer>
            );
          } else {
            return <Login />;
          }
        }}
      </UserContext.Consumer>
    </UserContextProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
export default App;
